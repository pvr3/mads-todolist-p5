

# --- !Ups

CREATE TABLE Friend(
	email varchar(255) NOT NULL,
	amigo varchar(255) NOT NULL,
	PRIMARY KEY(email, amigo)
);


# --- !Downs
DROP TABLE Friend;