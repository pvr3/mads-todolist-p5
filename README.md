## Equipo 8

[Bitbucket](https://bitbucket.org/pvr3/mads-todolist-p5)

- LOPEZ CORDERO, JAVIER
- VARELA GANDIA, PABLO
- VERDU ROMERO, PABLO

### Comentarios rápidos

- Ramas `master` y `develop`. Tags `1.0`, `1.2`, `5.0` y `5.0.1`
- Bien el TDD
- Bien el desarrollo de característica en Git
- Algo escasas las nuevas funcionalidades
- **Falta subir la presentación**

Buen trabajo en general, enhorabuena.

Calificación: **0,6**

7 de enero de 2014  
Domingo Gallardo

----

**Práctica 5: Desarrollo en grupo de nuevas funcionalidades**


Componentes del grupo:

- Javier López Cordero
- Pablo Varela Gandia
- Pablo Verdú Romero


URL: http://mads-todolist-p5.herokuapp.com/

----------



1. Especificación de las historias de usuario
	
	- Primera característica

			Lista de amigos
			
			Como **Usuario**
			quiero poder agregar amigos y tenerlos en una lista 
			para ver a mis amigos.
			5/12/2013

	- Segunda característica
	
			Compartir tareas

			Como **Usuario** quiero poder compartir tareas con los usuarios 
			para agilizar el trabajo.
			5/12/2013



2. Criterios de aceptación de la historia
	- Lista de amigos
		- En el modelo debe haber un listado de amigos dado un email de usuario.
		- En el modelo debe haber un método para añadir un amigo dado un email del creado y un email del usuario.
		- Para poder agregar y listar amigos debe estar logueado el usuario.
		- El usuario debe conocer el email del amigo que quiere agregar.
		- El amigo debe existir en la base de datos para poder agregarlo.
		- Solo se mostraran en la vista del usuario los amigos que tiene agregados.
		- No se puede agregar un amigo con el campo vacio.
	- Compartir tareas
		- Para crear una tarea y compartirla tiene que estar logueado.
		- Al crear una tarea está debe ser pública o privada.
		- Se listará añadiendo una tabla con las tareas que sean publicas.
		- Si haces una tarea pública todos los usuarios podrán ver tu tarea.
		- Si haces una tarea privada solo debe aparecer en tu lista de tareas.
		- En el modelo debe aparecer una lista con tareas públicas y privadas de un usuario en concreto.

----------