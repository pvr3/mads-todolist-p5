package models

import anorm._
import anorm.SqlParser._
import play.api.db._
import play.api.Play.current

case class Friend(emailFrom: String, emailTo: String)

object Friend {

	var friend = {
		get[String]("email") ~
		get[String]("amigo") map {
			case emailFrom~emailTo => Friend(emailFrom,emailTo)
		}
	}

	def addFriend(friend: Friend) = {
		DB.withConnection { implicit conn =>
			SQL( "insert into Friend (email, amigo) values ({email}, {amigo})" ).on(
				'email -> friend.emailFrom,
				'amigo -> friend.emailTo 
			).executeUpdate()				    
		}		
	}

	def myFriends(emailFrom: String): List[Friend] = {
		DB.withConnection { implicit conn =>
		    SQL("""select * from Friend 
		    	where email = {email}""").on(
				'email -> emailFrom).as(friend *)
		}
	}

}