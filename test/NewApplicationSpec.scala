package test

import org.specs2.mutable._

import play.api.test._
import play.api.test.Helpers._

class NewApplicationSpec extends Specification {

	"Application" should {
		"render index page with list of users" in {
			running(FakeApplication()) {
				val home = route(FakeRequest(GET, "/tasks").withSession("email" -> "pepe@ua.es")).get
				
				status(home) must equalTo(OK)
				contentType(home) must beSome.which(_ == "text/html")
		        contentAsString(home) must contain ("List of friends")
		        contentAsString(home) must contain ("friend(s)")
			}
		}
		
		"add new friend" in {
			running(FakeApplication()) {
				val add = route(FakeRequest(POST, "/addFriend").withSession("email" -> "pepe@ua.es").withFormUrlEncodedBody(("emailFrom", "pepe@ua.es"),("emailTo", "luis@ua.es"))).get
				
				status(add) must equalTo(SEE_OTHER)
				//redirectLocation(add).get must equalTo("/tasks")
			}
		}
		
		"render index page with a new friend" in {
			running(FakeApplication()) {
				val add = route(FakeRequest(POST, "/addFriend").withSession("email" -> "pepe@ua.es").withFormUrlEncodedBody(("emailFrom", "pepe@ua.es"),("emailTo", "luis@ua.es"))).get
				status(add) must equalTo(SEE_OTHER)
				redirectLocation(add).get must equalTo("/tasks")

				val tasks = route(FakeRequest(GET, "/tasks").withSession("email" -> "pepe@ua.es")).get

			    contentType(tasks) must beSome.which(_ == "text/html")
			    contentAsString(tasks) must contain ("List of friends")
			    contentAsString(tasks) must contain ("luis@ua.es")
			}
		}
		
		"add new friend with empty label" in {
			running(FakeApplication()) {
				val emptyLabel = route(FakeRequest(POST, "/addFriend").withSession("email" -> "pepe@ua.es").withFormUrlEncodedBody(("emailFrom", "pepe@ua.es"),("emailTo", ""))).get
				status(emptyLabel) must equalTo(BAD_REQUEST)
		        contentType(emptyLabel) must beSome.which(_ == "text/html") 
		        //contentAsString(emptyLabel) must contain ("This field is required")
			}
		}

		"render a public task to all users" in {
			running(FakeApplication()){
				val createPublic = route(FakeRequest(POST, "/tasks/newTask").withSession("email" -> "pepe@ua.es").withFormUrlEncodedBody(("label","Tarea publica"),("endDate",""),("user", "pepe@ua.es"),("visibility", "true"))).get

				val otherUser = route(FakeRequest(GET, "/tasks").withSession("email" -> "luis@ua.es")).get
				contentAsString(otherUser) must contain ("Public tasks")
				contentAsString(otherUser) must contain ("Tarea publica")
				contentAsString(otherUser) must contain ("pepe@ua.es")
			}
		}

		"not render a private task to all users" in {
			running(FakeApplication()){
				val createPublic = route(FakeRequest(POST, "/tasks/newTask").withSession("email" -> "pepe@ua.es").withFormUrlEncodedBody(("label","Tarea privada"),("endDate",""),("user", "pepe@ua.es"),("visibility", "false"))).get

				val otherUser = route(FakeRequest(GET, "/tasks").withSession("email" -> "luis@ua.es")).get
				contentAsString(otherUser) must contain ("Public tasks")
				contentAsString(otherUser) must not contain ("Tarea privada")
			}
		}
    }
}