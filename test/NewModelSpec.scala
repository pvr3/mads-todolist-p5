package test

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import anorm._ 

import models.User
import models.Task
import models.Friend

class NewModelSpec() extends Specification {

	def dateIs(date: java.util.Date, str: String) = new java.text.SimpleDateFormat("yyyy-MM-dd").format(date) == str
	def strToDate(str: String) = new java.text.SimpleDateFormat("yyyy-MM-dd").parse(str)

	"Friend" should {
		"listar los amigos del usuario" in {
		 running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
		 	User.add(User("pedro@root.va", "1234"))
		 	// San Pedro friends
		 	User.add(User("mateo@root.va", "1234"))
		 	User.add(User("judasTadeo@root.va", "1234"))
		 	// Add friends from pedro
		 	Friend.addFriend(Friend("pedro@root.va", "mateo@root.va"))
		 	Friend.addFriend(Friend("pedro@root.va", "judasTadeo@root.va"))
		 	val listOfFriends:List[Friend] = Friend.myFriends("pedro@root.va")
		 	listOfFriends(1).emailTo must equalTo("mateo@root.va")
		 	listOfFriends(0).emailTo must equalTo("judasTadeo@root.va")
		 }
		}
		
		"Añadir un amigo a la lista" in {
			running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
				User.add(User("bueno@ua.es","bueno"))
				// Bueno's friend
				User.add(User("maloso@ua.es","maloso"))
		        Friend.addFriend(Friend("bueno@ua.es","maloso@ua.es"))
				//Comprobamos si lo tiene como amigo
				var listOfFriends:List[Friend] = Friend.myFriends("bueno@ua.es")
		 		listOfFriends(0).emailTo must equalTo("maloso@ua.es")
			}
		}

	}

	"Task" should {
		"Ser privada o publica" in {
			running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
			Task.create(new Task(anorm.NotAssigned,"Comprar huevos", Option(strToDate("2013-11-14")), "pepe@ua.es", true))
			Task.create(new Task(anorm.NotAssigned,"Limpiar platos", Option(strToDate("2013-11-14")), "pepe@ua.es", false))
			//publicas
			val listPublic:List[Task] = Task.all(None)
			listPublic(0).visibility must beTrue
			//privadas
			val listPrivate:List[Task] = Task.listByUser("pepe@ua.es",None)
			listPrivate(0).visibility must beFalse



			}
		}
	}
}